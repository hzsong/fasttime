// fast time written by pure rust.
// by huangzhisong and guoyu in 2023-0918

#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

#[macro_use]
extern crate log;
// #[macro_use]
// extern crate serde;
// #[macro_use]
// extern crate serde_json;

pub mod a1_year;
pub mod a1_year_utils;
pub mod a2_month;
pub mod a4_weekday;
pub mod a5_timezone;

/// 基准值, base on chrono
pub mod a6_timestamp;
pub mod a7_secs;
pub mod a8_micros;
/// ellapse between two TimeFT or two DateTimeFT
pub mod a9_ellapse;


/// 纯日期
pub mod b1_date;
pub mod b1_date_utils;
/// 纯时间
pub mod b2_time;
/// 本地(墙上)时间
pub mod b3_datetime;
/// utils for chrono
pub mod u1_chrono;
/// utils for int value, 整数求余 
pub(crate) mod u2_int;

#[cfg(test)]
pub(crate) mod test_suites;

