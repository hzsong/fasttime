


use once_cell::sync::Lazy;

use crate::a1_year::IYear;




pub const N_MONTH: usize = 12;

pub static DAYS_LEAP: Lazy<[u8; N_MONTH]> = Lazy::new(|| {
    [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
});

pub static DAYS_NONLEAP: Lazy<[u8; N_MONTH]> = Lazy::new(|| {
    [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
});


pub static LIST_MONTH_DAY_LEAP: Lazy<Vec<(u8, u8)>> = Lazy::new(|| {
    let mut list_month_day_leap = vec![];

    let mut month = 1;
    for days in DAYS_LEAP.iter() {
        for day in 1..(*days + 1) {
            list_month_day_leap.push((month, day));
        }
        month += 1;
    }

    list_month_day_leap
});

pub fn days_inmonth(year: i32, month: u8) -> u8 {
    if month == 0 || month > 12 {
        return 0;
    }
    if month != 2 {
        return DAYS_LEAP[(month - 1) as usize];
    }
    if year.is_leap() {
        29
    } else {
        28
    }
}


pub static LIST_MONTH_DAY_NONLEAP: Lazy<Vec<(u8, u8)>> = Lazy::new(|| {
    let mut list_month_day_nonleap = vec![];

    let mut month = 1;
    for days in DAYS_NONLEAP.iter() {
        for day in 1..(*days + 1) {
            list_month_day_nonleap.push((month, day));
        }
        month += 1;
    }

    list_month_day_nonleap
});

#[test]
fn test_indexof_date_thisyear() {
    use crate::b1_date::DateFT;
    use crate::b1_date_utils;
    // 2023, non-leap year
    // 2024, leep year
    for year in vec![2023, 2024] {
        for month in 1..13 {
            let days = days_inmonth(year, month);
            for day in 1..(days + 1) {
                let date = DateFT::from_ymd_unsafe(year, month, day);
                let index = b1_date_utils::index_inyear(&date);
                // let day_inyear = day_inyear(&date);
                // let day_inyear = index + 1;
                println!("{}: {}", index + 1, date.to_print_9());
            }
        }
    }
}

#[test]
fn test_print_list_monthday() {
    use std::ops::Deref;
    println!("正常年份:");
    let mut idx = 1;
    for (month, day) in LIST_MONTH_DAY_NONLEAP.deref() {
        println!("\t{idx}: {month:0>2} 月 {day:0>2} 日");
        idx += 1;
    }
    println!("闰年年份:");
    let mut idx = 1;
    for (month, day) in LIST_MONTH_DAY_LEAP.deref() {
        println!("\t{idx}: {month:0>2} 月 {day:0>2} 日");
        idx += 1;
    }
}



