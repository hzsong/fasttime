use fast_able::map_hash::SyncHashMap;
use once_cell::sync::Lazy;

use crate::{a1_year::IYear, a6_timestamp};


static MAP_DAYS_FROM_19700101: Lazy<SyncHashMap<i32, i32>> = Lazy::new(|| {
    let map_year_daysfrom19700101 = SyncHashMap::new();
    
    let mut days_total = 0;
    for year in (1900..1970).rev() {
        days_total -= year.days();
        map_year_daysfrom19700101.insert(year, days_total);
    }

    let mut days_total = 0;
    map_year_daysfrom19700101.insert(1970, days_total);
    let year_today = a6_timestamp::BASIC_INFO_STARTUP.date.year();
    for year in 1971..year_today + 2 {
        let year_previous = year - 1;
        days_total += year_previous.days();
        map_year_daysfrom19700101.insert(year, days_total);
    }

    map_year_daysfrom19700101
});




#[test]
fn generate_DAYS_FROM_19700101() {
    use crate::a4_weekday::Weekday;
    let mut years = MAP_DAYS_FROM_19700101.iter().map(|x| *x.0 ).collect::<Vec<i32>>();
    years.sort();

    for year_val in years {
        let days = MAP_DAYS_FROM_19700101.get(&year_val).unwrap();
        let weekday_1月1日 = Weekday::Thu.add_days(*days);
        println!("{year_val}-01-01, 周 {weekday_1月1日}: 距 1970-0101 {days} 天.");
    }
}

// 1900-01-01: Mon
// 1923-01-01: Mon, 2023能看到的最早日历时间
// 1970-01-01: Thu, 系统时间戳的基准日期
// 2000-01-01: Sat
// 2023-01-01, Sun


#[test]
fn generate_每年1月1日是周几() {
    use crate::a4_weekday::Weekday;
    let year_val = 1923;
    let mut weekday_1月1日 = Weekday::Mon;
    for year in (1900..1923).rev() {
        let days = year.days();
        weekday_1月1日 = weekday_1月1日.add_days(-days);
        println!("{year}-01-01: {weekday_1月1日:?}");
    }
    
    let year_today = a6_timestamp::BASIC_INFO_STARTUP.date.year();
    
    let mut weekday_1月1日 = Weekday::Mon;
    for year in 1923..year_today + 1 {
        let year_previous = year - 1;
        let days = year_previous.days();
        weekday_1月1日 = weekday_1月1日.add_days(days);
        println!("{}-01-01: {weekday_1月1日:?}", year_val + 1);
    }
}