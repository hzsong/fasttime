


pub trait IYear {
    fn is_leap(&self) -> bool;
    fn days(&self) -> i32;
}


impl IYear for i32 {
    fn is_leap(&self) -> bool {
        let year = *self;
        if year % 100 == 0 {
            year % 400 == 0
        } else {
            year % 4 == 0
        }
    }
    fn days(&self) -> i32 {
        if self.is_leap() {
            366
        } else {
            365
        }
    }
}