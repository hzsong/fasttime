use std::ops::Deref;

use crate::{b1_date::DateFT, a2_month, a1_year::IYear};



pub fn days_inyear(date: &DateFT) -> usize {
    index_inyear(date) + 1
}

pub fn index_inyear(date: &DateFT) -> usize {
    let (year, month, day) = date.ymd();
    let list_days = if year.is_leap() {
        a2_month::DAYS_LEAP.deref()
    } else {
        a2_month::DAYS_NONLEAP.deref()
    };
    let mut index = 0;
    let mut month_loop = 1;
    for days in list_days {
        if month_loop == month {
            let day_diff = day - 1;
            index += day_diff as usize;
            break;
        }
        month_loop += 1;
        index += *days as usize;
    }

    index

}