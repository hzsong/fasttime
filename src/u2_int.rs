
// 整数求余 

/// divisor(除数), dividend(被除数)
pub fn divide_remain_i32(divisor: i32, dividend: i32) -> (i32, i32) {
    if dividend == 0 {
         panic!("divide_remain_i32: dividend can't be 0");
    }
    let residue = divisor % dividend;
    let quotient = divisor / dividend;
    if residue < 0 {
        if dividend < 0 {
            (quotient + 1, residue - dividend)
        } else {
            (quotient - 1, residue + dividend)
        }
    } else {
        (quotient, residue)
    }

}

/// divisor(除数), dividend(被除数)
pub fn divide_remain_u32(divisor: u32, dividend: u32) -> (u32, u32) {
    if dividend == 0 {
         panic!("divide_remain_u32: dividend can't be 0");
    }
    let quotient = divisor / dividend;
    let residue = divisor % dividend;

    (quotient, residue)
}


/// divisor(除数), dividend(被除数)
pub fn divide_remain_i64(divisor: i64, dividend: i64) -> (i64, i64) {
    if dividend == 0 {
         panic!("divide_remain_i64: dividend can't be 0");
    }
    let quotient = divisor / dividend;
    let residue = divisor % dividend;

    if residue < 0 {
        if dividend < 0 {
            (quotient + 1, residue - dividend)
        } else {
            (quotient - 1, residue + dividend)
        }
    } else {
        (quotient, residue)
    }
}

/// divisor(除数), dividend(被除数)
pub fn divide_remain_u64(divisor: u64, dividend: u64) -> (u64, u64) {
    if dividend == 0 {
         panic!("divide_remain_u64: dividend can't be 0");
    }
    let quotient = divisor / dividend;
    let residue = divisor % dividend;

    (quotient, residue)
}

#[test]
fn test_minus_divide_remain() {
    let dividend = 24;
    let dividend_minus = -24;
    for divisor in -50..50 {
        let (quotient, residue) = divide_remain_i64(divisor, dividend);
        println!("{divisor} / {dividend} = {quotient}(商) {residue}(余);");
        let (quotient, residue) = divide_remain_i64(divisor, dividend_minus);
        println!("{divisor} / {dividend_minus} = {quotient}(商) {residue}(余);");
        println!();
    }
}