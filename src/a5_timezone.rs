use parse_display::Display;


#[repr(i16)]
#[derive(serde::Deserialize, serde::Serialize, Debug, Display, Clone, Copy, PartialEq, Eq)]
pub enum TimeZone {
    /// 零时区, 国际子午线
    UTC_00 = 0,
    UTC_E01 = 1,
    UTC_E02 = 2,
    UTC_E03 = 3,
    UTC_E04 = 4,
    UTC_E05 = 5,
    UTC_E06 = 6,
    UTC_E07 = 7,
    UTC_E08 = 8,
    UTC_E09 = 9,
    UTC_E10 = 10,
    UTC_E11 = 11,
    UTC_EW12 = 12,
    UTC_W11 = -11,
    UTC_W10 = -10,
    UTC_W09 = -9,
    UTC_W08 = -8,
    UTC_W07 = -7,
    UTC_W06 = -6,
    UTC_W05 = -5,
    UTC_W04 = -4,
    UTC_W03 = -3,
    UTC_W02 = -2,
    UTC_W01 = -1,
}
impl Default for TimeZone {
    fn default() -> Self {
        Self::UTC_00
    }
}

impl TimeZone {
    pub fn from_i32(val: i32) -> Self {
        match val {
            0 => Self::UTC_00,
            1 => Self::UTC_E01,
            2 => Self::UTC_E02,
            3 => Self::UTC_E03,
            4 => Self::UTC_E04,
            5 => Self::UTC_E05,
            6 => Self::UTC_E06,
            7 => Self::UTC_E07,
            8 => Self::UTC_E08,
            9 => Self::UTC_E09,
            10 => Self::UTC_E10,
            11 => Self::UTC_E11,
            12 => Self::UTC_EW12,
            -11 => Self::UTC_W11,
            -10 => Self::UTC_W10,
            -9 => Self::UTC_W09,
            -8 => Self::UTC_W08,
            -7 => Self::UTC_W07,
            -6 => Self::UTC_W06,
            -5 => Self::UTC_W05,
            -4 => Self::UTC_W04,
            -3 => Self::UTC_W03,
            -2 => Self::UTC_W02,
            -1 => Self::UTC_W01,
            _ => Self::UTC_00,
        }
    }
    pub fn hours_offset(&self) -> i32 {
        match self {
            TimeZone::UTC_00 => 0,
            TimeZone::UTC_E01 => 1,
            TimeZone::UTC_E02 => 2,
            TimeZone::UTC_E03 => 3,
            TimeZone::UTC_E04 => 4,
            TimeZone::UTC_E05 => 5,
            TimeZone::UTC_E06 => 6,
            TimeZone::UTC_E07 => 7,
            TimeZone::UTC_E08 => 8,
            TimeZone::UTC_E09 => 9,
            TimeZone::UTC_E10 => 10,
            TimeZone::UTC_E11 => 11,
            TimeZone::UTC_EW12 => 12,
            TimeZone::UTC_W11 => -11,
            TimeZone::UTC_W10 => -10,
            TimeZone::UTC_W09 => -9,
            TimeZone::UTC_W08 => -8,
            TimeZone::UTC_W07 => -7,
            TimeZone::UTC_W06 => -6,
            TimeZone::UTC_W05 => -5,
            TimeZone::UTC_W04 => -4,
            TimeZone::UTC_W03 => -3,
            TimeZone::UTC_W02 => -2,
            TimeZone::UTC_W01 => -1,
        }
    }
}
