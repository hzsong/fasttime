use parse_display::Display;

use crate::u2_int;



#[repr(u8)]
#[derive(PartialEq, Eq, Copy, Clone, Debug, Hash, Display)]
pub enum Weekday {
    /// Monday.
    Mon = 0,
    /// Tuesday.
    Tue = 1,
    /// Wednesday.
    Wed = 2,
    /// Thursday.
    Thu = 3,
    /// Friday.
    Fri = 4,
    /// Saturday.
    Sat = 5,
    /// Sunday.
    Sun = 6,
}

impl Weekday {
    pub const DAYS_ONEWEEK: u8 = 7;

    pub fn from_en(val: u8) -> Self {
        let val = if val < Self::DAYS_ONEWEEK {
            val
        } else {
            val % Self::DAYS_ONEWEEK
        };
        match val {
            0 => Self::Mon,
            1 => Self::Tue,
            2 => Self::Wed,
            3 => Self::Thu,
            4 => Self::Fri,
            5 => Self::Sat,
            _ => Self::Sun,
        }
    }

    pub fn val_en(&self) -> u8 {
        match self {
            Self::Mon => 0,
            Self::Tue => 1,
            Self::Wed => 2,
            Self::Thu => 3,
            Self::Fri => 4,
            Self::Sat => 5,
            Self::Sun => 6,
        }
    }
    pub fn from_cn(val: u8) -> Self {
        if val == 0 {
            return Self::Sun;
        };
        Self::from_en(val - 1)
    }
    pub fn val_cn(&self) -> u8 {
        self.val_en() + 1
    }

    pub fn add_days(&self, days: i32) -> Self {
        let (_weeks, days_remain) = u2_int::divide_remain_i32(days, Self::DAYS_ONEWEEK as i32);
        let val_en = self.val_en() + days_remain as u8;
        Self::from_en(val_en)
    }
}