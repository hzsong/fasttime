
#[test]
fn test_chrono_timestamp() {
    use std::{ops::Deref, str::FromStr};

    use chrono::NaiveDateTime;

    use crate::{a1_year::IYear, a2_month, a7_secs, test_suites};


    let mut buf = "Run u1_chrono.rs::test_chrono_timestamp:\n".to_string();

    let mut secs = 0;
    const secs_per_day: i64 = a7_secs::SECS_PER_DAY as i64;
    for year in 1970..2024 {
        let list_month_day = if year.is_leap() {
            a2_month::LIST_MONTH_DAY_LEAP.deref()
        } else {
            a2_month::LIST_MONTH_DAY_NONLEAP.deref()
        };
        for (month, day) in list_month_day {
            let str_today = format!("{year}-{month:0>2}-{day:0>2}T00:00:00");
            let navie_today = NaiveDateTime::from_str(&str_today).expect("Unreachable");
            let timestamp_micros = navie_today.timestamp_micros();
            let timestamp_secs = timestamp_micros / 1_000_000;
            buf.push_str(&format!("\t{str_today} timestamp: {timestamp_secs} secs, {timestamp_micros} micros;\n"));
            if timestamp_secs != secs {
                buf.push_str(&format!("\t\t{timestamp_secs} - {secs} = {} secs leap.\n", timestamp_secs - secs));
            }
            secs += secs_per_day;
        }
    }
    test_suites::write_utf8("./test_results/test_chrono_timestamp.txt", buf);
    
}