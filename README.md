# fasttime

#### 介绍
Fast datetime library written in pure rust, by huangzhisong and guoyu in 2023-10 at Lufeng, Yunnan, China.

#### 软件架构
FastTime, 纯Rust编写的快速时间库, 并发: 2800万+/秒.
一、组件:
1. 年份: a1_year.rs, 含闰年
2. 月份: a2_month.rs
3. 星期: a4_weekday.rs
4. 时区: a5_timezone.rs
5. 时间戳: a6_timestamp.rs
6. 日内秒: a7_secs.rs, 最小值 0=00:00:00, 最大值 86399=23:59:59, 不含闰秒
7. 微秒数(不足1秒部分): a8_micros.rs, (毫秒数)微秒数, 最小值: 0=(000)000, 最大值999_999=(999)999
8. 时间差: a9_ellapse.rs: 精确到微秒
9. 日期: b1_date.rs: 年(i32), 月(u8, 1-12), 日(u8, 1-28,29,30,31)
10. 时间: b2_time.rs, 组成: 6.日内秒 + 7.微秒数(不足1秒部分)
11. 日期-时间: b3_datetime.rs, 组成: 4. 时区 + 5. 时间戳 + 9. 日期 + 10. 时间
二、其他
12. 测试样例集合: test_suites.rs
13. 测试样例其他: 分散在.rs文件中，请搜索: fn test_
14. 测试样例数据(行数很多): test_results/*.txt(.zip)
作者：hzsong, 2023-11-04
邮箱: hzsong123@126.com
仓库地址: https://gitee.com/hzsong/fasttime.

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
